import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { ApiService } from './api.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ShowListComponent } from './shows/show-list/show-list.component';
import { ShowModalComponent } from './show-modal/show-modal.component';
import { FormsModule } from '@angular/forms';
import { JwtModule } from '@auth0/angular-jwt';
import { AuthGuard } from './guards/auth-guard.service';
import { HttpErrorInterceptor } from './interceptors/http-error-interceptor.service';

export function getToken() {
	return localStorage.getItem("jwt");
}

@NgModule({
  declarations: [
    AppComponent,
    ShowListComponent,
    ShowModalComponent,
    routingComponents
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
		HttpClientModule,
		FormsModule,
		JwtModule.forRoot({
			config: {
				tokenGetter: getToken,
				allowedDomains: ["http://localhost:21772"],
				disallowedRoutes: []
			}
		})
  ],
  providers: [
		ApiService, 
		AuthGuard,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: HttpErrorInterceptor,
			multi: true,
		}
	],
  bootstrap: [AppComponent]
})
export class AppModule { }
