import { UserRole } from "./user-role.model";

export class User
{
	public userID: number;
	public username: string;
	public password: string;
	public role: UserRole;
}