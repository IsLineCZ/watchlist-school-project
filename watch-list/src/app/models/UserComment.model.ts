export class UserComment
{
	public userCommentID: number;
	public userID: number;
	public username: string;
	public showID: number;
	public text: string;
}