export enum ShowGenre {
	ActionAndAdventure = "Action And Adventure",
	Animation = "Animation",
	Comedy = "Comedy",
	Crime = "Crime",
	Documentary = "Documentary",
	Drama = "Drama",
	Family = "Family",
	Kids = "Kids",
	Mystery = "Mystery",
	News = "News",
	Reality = "Reality",
	ScifiAndFantasy = "Scifi And Fantasy",
	Soap = "Soap",
	WarAndPolitics = "War And Politics",
	Talk = "Talk",
	Western = "Western"
}