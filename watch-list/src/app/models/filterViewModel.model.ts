export class FilterViewModel
{
	public normalizedName: string;
	public minRating: number;
	public maxRating: number;
	public genre: string;
	public page: number;
}