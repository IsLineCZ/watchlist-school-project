export enum ShowState {
  Watching = "Watching",
	OnHold = "On-Hold",
	PlanToWatch = "Plan To Watch",
	Dropped = "Dropped",
	Completed = "Completed"
}