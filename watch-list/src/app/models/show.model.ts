import { UserComment } from "./UserComment.model";

export class Show
{
	public showID: number;
	public name: string;
	public backdropPath: string;
	public firstAirDate: Date;
	public originalLanguage: string;
	public originalName: string;
	public overview: string;
	public genre: string;
	public popularity: number;
	public posterUrl: string;
	public rating: number;
	public numberOfRatings: number;
	public comments: UserComment[];
}