import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from './api.service';
import { LoginViewModel } from './models/loginViewModel.model';
import { UserRole } from './models/user-role.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
	constructor(private apiService: ApiService, private router: Router) {}

	loggedInUser: string;
	isAdmin = false;

	ngOnInit(): void {
		this.apiService.getUser().subscribe((user) => {
			if (user === null) {
				this.router.navigate(["login"]);
			} 
			else {
				this.loggedInUser = user.username;
				this.isAdmin = user.role === UserRole.Admin;
				this.router.navigate(["shows"]);
			}
		})
	}

	logout() {
		this.apiService.logoutUser().subscribe();
		localStorage.removeItem("jwt");
		this.loggedInUser = "";
		this.router.navigate(["login"]);
	}

	fillDatabase() {
		this.apiService.fillDatabase().subscribe((result) => console.log(result));
	}
}
