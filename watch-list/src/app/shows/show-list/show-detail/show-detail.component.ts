import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ChildActivationStart } from '@angular/router';
import { ApiService } from 'src/app/api.service';
import { UserComment } from 'src/app/models/UserComment.model';
import { Show } from 'src/app/models/show.model';
import { User } from 'src/app/models/User.mode';
import { UserRole } from 'src/app/models/user-role.model';
import * as moment from 'moment';

@Component({
  selector: 'app-show-detail',
  templateUrl: './show-detail.component.html',
  styleUrls: ['./show-detail.component.css']
})
export class ShowDetailComponent implements OnInit {
	
  constructor(private route: ActivatedRoute, private apiService: ApiService) { }

	show: Show;
	comment: string;
	user: User;
	isAdmin = false;

  ngOnInit(): void {
		this.apiService.getUser().subscribe((user) => {
			this.user = user;
			this.isAdmin = this.user.role === UserRole.Admin;
		});
		this.getShow();
  }

	getShow() {
		window.scrollTo(0, 0);
		this.route.params.subscribe((params) => {
			this.apiService.getShow(Number(params.id)).subscribe((show) => this.show = show);
		});
	}

	getFormatedDate(firstAirDate: Date | undefined): string {
		return firstAirDate === undefined ? "" : moment(firstAirDate).format('MM/DD/YYYY');
	}

	addComment() {
		if (this.comment === undefined || this.comment === null || this.comment.length === 0) return;
		const comment = new UserComment();
		comment.text = this.comment;
		comment.userID = this.user.userID;
		comment.username = this.user.username;
		comment.showID = this.show.showID;

		if (this.show.comments === null || this.show.comments === undefined) this.show.comments = [];

		this.show.comments.push(comment);
		this.apiService.updateShow(this.show).subscribe(() => {
			this.getShow();
			this.comment = "";
		});
	}

	removeComment(id: number) {
		this.show.comments = this.show.comments.filter(comment => comment.userCommentID !== id);
		this.apiService.deleteComment(id).subscribe(() => {
			this.apiService.updateShow(this.show).subscribe(() => {
				this.getShow();
			});
		});
	}
}
