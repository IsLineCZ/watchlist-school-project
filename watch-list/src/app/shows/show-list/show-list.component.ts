import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { ModalType } from 'src/app/models/modal-type.model';
import { Show } from 'src/app/models/show.model';
import { ShowState } from 'src/app/models/show-state.model';
import { ShowGenre } from 'src/app/models/show-genre.model';
import { FilterViewModel } from 'src/app/models/filterViewModel.model';
import { UserRole } from 'src/app/models/user-role.model';

@Component({
  selector: 'show-list',
  templateUrl: './show-list.component.html',
  styleUrls: ['./show-list.component.css']
})
export class ShowListComponent implements OnInit {

  constructor(private apiService: ApiService) {}

	shows: Show[];
	show: Show;
	filterParams = new FilterViewModel();
	filterName: string;
	displayShowModal: boolean;
	hideFilters: boolean = true;
	modalTitle: string;
	modalType: ModalType;
	favouriteShowName: string;
	stateList = ShowState;
	genreList = ShowGenre;
	isAdmin = false;
	page = 1;
	pageList: number[];
	maxPageSize: number;

	ngOnInit(): void {
		this.apiService.getUser().subscribe((user) => this.isAdmin = user.role === UserRole.Admin)
		this.apiService.getShowCount().subscribe((count) => {
			this.maxPageSize = Math.ceil(count / 20);
			this.getPageList(1, this.maxPageSize < 20 ? this.maxPageSize : 20);
		});
		this.getShows(this.page);
		this.filterParams.minRating = 0;
		this.filterParams.maxRating = 10;
		this.filterParams.page = this.page;
	}

	getShows(page: number): void {
		this.apiService.getShows(page).subscribe((shows) => {
			this.shows = shows;
			const favouriteShowResult = shows.find(show => show.rating === Math.max.apply(Math, shows.map(function(show) {return show.rating})));
			if (favouriteShowResult) this.favouriteShowName = favouriteShowResult.name;
		});
	}

	getShowYear(firstAirDate: Date): number {
		return new Date(firstAirDate).getFullYear();
	}

	editShow(id: number) {
		const show = this.shows.find(show => show.showID === id);

		if (show) {
			this.modalTitle = 'Edit show';
			this.displayShowModal = true;
			this.modalType = ModalType.Edit;
			this.show = show;
		}
	}

	addShow() {
		this.modalTitle = 'Add show';
		this.displayShowModal = true;
		this.modalType = ModalType.Add;
		this.show = new Show();
	}

	deleteShow(id: number) {
		this.apiService.deleteShow(id).subscribe(() => {
			this.hideFilters ? this.getShows(this.page) : this.filterShows();
		});
	}

	toggleFilters() {
		this.hideFilters = this.hideFilters ? false : true;
	}

	clearFilters() {
		this.filterParams = new FilterViewModel();
		this.filterName = "";
		this.filterParams.minRating = 0;
		this.filterParams.maxRating = 10;
		this.getShows(this.page);
	}

	filterShows() {
		this.checkMinMaxFilter();
		this.filterParams.normalizedName = this.normalize(this.filterName);
		this.apiService.filterShows(this.filterParams).subscribe((shows) => this.shows = shows);
	}

	normalize(value: string) {
		return value ? value.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toUpperCase() : "";
	}

	private checkMinMaxFilter() {
		if (this.filterParams.minRating > this.filterParams.maxRating) {
			const tmp = this.filterParams.minRating;
			this.filterParams.minRating = this.filterParams.maxRating;
			this.filterParams.maxRating = tmp;
		}

		if (this.filterParams.minRating == null || this.filterParams.minRating < 0) this.filterParams.minRating = 0;
		if (this.filterParams.maxRating == null || this.filterParams.maxRating > 10) this.filterParams.maxRating = 10;
	}

	getPageList(start: number, end: number) {
		this.pageList = [...Array(end - start + 1).keys()].map(number => number + start);
	}

	pickPage(page: number) {
		this.page = page;
		this.filterParams.page = page;
		var start = 1;
		const end = 20;
		const endOffset = 3;

		if (this.page >= end - endOffset) {
			start = this.page - end + endOffset + 1;
			this.getPageList(start, this.page + endOffset <= this.maxPageSize ? this.page + endOffset : this.maxPageSize);
		}
		if (this.page <= end - endOffset) {
			start = 1;
			this.getPageList(start, this.maxPageSize < 20 ? this.maxPageSize : 20);
		}

		this.getShowsAfterAction();
		window.scrollTo(0, 0);
	}

	getShowsAfterAction() {
		this.hideFilters ? this.getShows(this.page) : this.filterShows();
	}

	activePage() {
		return this.page;
	}

	showID(showID: number) {
    return showID;
	}
}
