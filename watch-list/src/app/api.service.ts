import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs'; 
import { Show } from './models/show.model';
import { RegistrationViewModel } from './models/registrationViewModel.model';
import { LoginViewModel } from './models/loginViewModel.model';
import { FilterViewModel } from './models/filterViewModel.model';
import { User } from './models/User.mode';
import { Token } from './models/token.model';
import { getToken } from './app.module';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
	readonly baseUrl = "http://localhost:21772";
	showList: Observable<Show[]>;
	latestPage: number;

  constructor(private httpClient: HttpClient) { }

	addShow(show: Show) {
		return this.httpClient.post(`${this.baseUrl}/show/add`, show);
	}

	getShow(id: number): Observable<Show> {
		return this.httpClient.post<Show>(`${this.baseUrl}/show/getShow`, id);
	}

	getShowCount(): Observable<number> {
		return this.httpClient.get<number>(`${this.baseUrl}/show/getShowCount`);
	}

	getShows(page: number): Observable<Show[]> {
		if (page === this.latestPage) {
			return this.showList;
		}
		else {
			this.showList = this.httpClient.get<Show[]>(`${this.baseUrl}/show/getShows/${page}`);
			this.latestPage = page;
			return this.showList;
		}
	}

	updateShow(show: Show) {
		return this.httpClient.post(`${this.baseUrl}/show/update`, show);
	}

	deleteShow(id: number) {
		return this.httpClient.post(`${this.baseUrl}/show/delete/${id}`, id);
	}

	filterShows(filterParams: FilterViewModel): Observable<Show[]> {
		return this.httpClient.post<Show[]>(`${this.baseUrl}/show/filter`, filterParams);
	}

	saveImage(image: FormData) {
		return this.httpClient.post(`${this.baseUrl}/show/saveImage`, image);
	}

	registerUser(registrationViewModel: RegistrationViewModel) {
		return this.httpClient.post(`${this.baseUrl}/account/register`, registrationViewModel);
	}

	loginUser(loginViewModel: LoginViewModel) {
		return this.httpClient.post(`${this.baseUrl}/account/login`, loginViewModel);
	}

	logoutUser() {
		return this.httpClient.post(`${this.baseUrl}/account/logout`, null);
	}

	getUser(): Observable<User> {
		const token = new Token();
		token.value = String(getToken());
		return this.httpClient.post<User>(`${this.baseUrl}/account/getUser`, token);
	}

	deleteComment(id: number) {
		return this.httpClient.post(`${this.baseUrl}/show/deleteComment/${id}`, id);
	}

	fillDatabase() {
		return this.httpClient.post(`${this.baseUrl}/show/fillWatchListDatabase`, null);
	}
}
