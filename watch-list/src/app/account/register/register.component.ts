import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/api.service';
import { routingComponents } from 'src/app/app-routing.module';
import { RegistrationViewModel } from 'src/app/models/registrationViewModel.model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private apiService: ApiService, private router: Router) { }

	registerData = new RegistrationViewModel();

  ngOnInit(): void {}

	register() {
		this.apiService.registerUser(this.registerData).subscribe((result) => {
			this.router.navigate(["login"]);
		}, error => {
			console.log(error);
		});
	}

}
