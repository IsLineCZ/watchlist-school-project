import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/api.service';
import { LoginViewModel } from 'src/app/models/loginViewModel.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private apiService: ApiService) { }

	loginData = new LoginViewModel();

  ngOnInit(): void {}

	login() {
		this.apiService.loginUser(this.loginData).subscribe((result) => {
			const token = (<any>result).token;
			localStorage.setItem("jwt", token);
			location.reload();
		}, error => {
			console.log(error);
		});
	}
}
