import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './account/login/login.component';
import { RegisterComponent } from './account/register/register.component';
import { AppComponent } from './app.component';
import { AuthGuard } from './guards/auth-guard.service';
import { ShowDetailComponent } from './shows/show-list/show-detail/show-detail.component';
import { ShowListComponent } from './shows/show-list/show-list.component';

const routes: Routes = [
	{ path: '', component: AppComponent },
	{ path: 'shows', component: ShowListComponent, canActivate: [AuthGuard] },
	{ path: 'shows/detail/:id', component: ShowDetailComponent, canActivate: [AuthGuard] },
	{ path: 'login', component: LoginComponent },
	{ path: 'register', component: RegisterComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [ AppComponent, ShowListComponent, ShowDetailComponent, LoginComponent, RegisterComponent ]
