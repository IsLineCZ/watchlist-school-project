import { HttpInterceptor, HttpRequest, HttpHandler, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators'
import * as alert from 'alertifyjs';

export class HttpErrorInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler) {
    return next.handle(request).pipe(
			catchError((error: HttpErrorResponse) => {
				alert.error(error.error);
				return throwError(error.error);
			}
		));
	}
}
