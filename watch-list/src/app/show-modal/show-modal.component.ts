import { Component, ElementRef, EventEmitter, Host, Input, OnInit, Output, ViewChild } from '@angular/core';
import { cloneDeep } from 'lodash';
import { ApiService } from 'src/app/api.service';
import { ModalType } from 'src/app/models/modal-type.model';
import { ShowGenre } from 'src/app/models/show-genre.model';
import { ShowState } from 'src/app/models/show-state.model';
import { Show } from 'src/app/models/show.model';

@Component({
  selector: 'show-modal',
  templateUrl: './show-modal.component.html',
  styleUrls: ['./show-modal.component.css']
})

export class ShowModalComponent implements OnInit {
	@Input() title: string;
	@Input() display: boolean;
	@Input() type: ModalType;
	@Input() set model(model: Show) {
		this._model = cloneDeep(model);
	}

	@Output() displayChange = new EventEmitter<boolean>();
	@Output() onSave = new EventEmitter<any>();

	@ViewChild('fileUpload') imageInputElement: ElementRef;
	selectedFile: File | null;
	fileUploadText = "Select show image";
	stateList = ShowState;
	genreList = ShowGenre;
	errorList: string[] = [];
	_model: Show;

  constructor(private apiService: ApiService) {}

  ngOnInit(): void {}

	close() {
		this.display = false;
		this.imageInputElement.nativeElement.value = '';
		this.selectedFile = null;
		this.fileUploadText = "Select show image";
		this.displayChange.emit(this.display);
		if (this.type === ModalType.Edit) this.onSave.emit();
	}

	save() {
		const formData = new FormData();

		if (!this.validate(this._model)) return;

		if (this.type === ModalType.Add) {
			if (!this.selectedFile) return;
			formData.append('image', this.selectedFile, this.selectedFile.name);

			this.apiService.saveImage(formData).subscribe((imageUrl) => {
				this._model.posterUrl = imageUrl.toString();
				this.apiService.addShow(this._model).subscribe((response) => {
					this.onSave.emit();
				});
			});
		}

		if (this.type === ModalType.Edit) {
			if (this.selectedFile) {
				formData.append('image', this.selectedFile, this.selectedFile.name);

				this.apiService.saveImage(formData).subscribe((imageUrl) => {
					this._model.posterUrl = imageUrl.toString();
					this.apiService.updateShow(this._model).subscribe((response) => {
						this.onSave.emit();
					});
				});
			}
			else {
				this.apiService.updateShow(this._model).subscribe(() => {
					this.onSave.emit();
				});
			}
		}

		this.close();
	}

	onFileSelected(event) {
		if (event.target.files[0]) {
			this.fileUploadText = "Image selected";
			this.selectedFile = event.target.files[0];
		}
	}

	private validate(show: Show): boolean {
		this.errorList = [];

		if (!show.name) this.errorList.push("Show name must be at least 1 character");
		else if (show.name?.length > 128) this.errorList.push(`Show name can have maximum of 128 characters. You have ${show.name.length}.`);
		if (!show.rating || show.rating < 0 || show.rating > 10) this.errorList.push("Show rating must be number 0 - 10.");
		if (!show.firstAirDate) this.errorList.push("Show first air date must be selected");
		if (!show.originalName) this.errorList.push("Show original name be at least 1 character");
		else if (show.originalName?.length > 128) this.errorList.push(`Show original name can have maximum of 128 characters. You have ${show.name.length}.`);
		if (!show.genre) this.errorList.push("Show genre must be selected.");
		if (!this.selectedFile && this.type === ModalType.Add) this.errorList.push("Show image must be selected.");
		if (!show.overview) this.errorList.push("Show overview must be at least 1 character");
		
		return this.errorList.length === 0;
	}
}