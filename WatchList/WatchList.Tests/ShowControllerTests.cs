using System.Net;
using Microsoft.AspNetCore.Mvc;
using WatchList.Controllers;
using WatchList.Services.Accounts;
using FakeItEasy;
using Xunit;
using Microsoft.AspNetCore.Hosting;
using WatchList.Services.Shows;

namespace WatchList.Tests
{
	public class ShowControllerTests
	{
		[Fact]
		public void DeleteShowWithWithoutPermission()
		{
			var enviroment = A.Fake<IWebHostEnvironment>();
			var showService = A.Fake<IShowService>();
			var accountService = A.Fake<IAccountService>();
			
			var controller = new ShowController(showService, enviroment, accountService);
			var response = controller.DeleteShow(10) as StatusCodeResult;

			Assert.Equal(HttpStatusCode.Unauthorized, (HttpStatusCode)response.StatusCode);
		}
	}
}
