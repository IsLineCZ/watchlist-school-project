using System.Net;
using FakeItEasy;
using Microsoft.AspNetCore.Mvc;
using WatchList.Controllers;
using WatchList.Models;
using WatchList.Services.Accounts;
using Xunit;

namespace WatchList.Tests
{
	public class LoginControllerTests
	{
		[Fact]
		public void EmptyRegistrationTest()
		{
			var registrationViewModel = new RegistrationViewModel()
			{
				Username = "",
				Password = null,
			};

			var accountService = A.Fake<IAccountService>();

			var controller = new AccountController(accountService);
			var response = controller.Register(registrationViewModel) as ObjectResult;

			Assert.Equal(HttpStatusCode.BadRequest, (HttpStatusCode)response.StatusCode);
		}
	}
}
