﻿namespace WatchList.Models
{
    public enum Genre
    {
        Action_And_Adventure = 10759,
		Animation = 16,
		Comedy = 35,
		Crime = 80,
		Documentary = 99,
		Drama = 18,
		Family = 10751,
		Kids = 10762,
		Mystery = 9648,
		News = 10763,
		Reality = 10764,
		Scifi_And_Fantasy = 10765,
		Soap = 10766,
		War_And_Politics = 10768,
		Talk = 10767,
		Western = 37,
    }
}