﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WatchList.Models
{
	public class Log
	{
		[Key]
		public int LogID { get; set; }

		public DateTime DateCreated { get; set; }

		public string Message { get; set; }

		public int? UserID { get; set; }
	}
}
