﻿namespace WatchList.Models
{
	public class FilterViewModel
	{
		public string NormalizedName { get; set; }

		public int MinRating { get; set; }

		public int MaxRating { get; set; }

		public string Genre { get; set; }

		public int Page { get; set; }
	}
}
