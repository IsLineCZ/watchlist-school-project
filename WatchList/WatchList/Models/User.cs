﻿using System.ComponentModel.DataAnnotations;

namespace WatchList.Models
{
	public class User
	{
		[Key]
		public int UserID { get; set; }

		[Required]
        [StringLength(128)]
        public string Username { get; set; }

		[Required]
        [StringLength(128)]
		public string Password { get; set; }

		[Required]
		public Role Role { get; set; }
	}
}
