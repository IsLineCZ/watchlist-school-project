﻿namespace WatchList.Models
{
	public class RegistrationViewModel
	{
		public string Username { get; set; }

		public string Password { get; set; }

		public string RepeatedPassword { get; set; }
	}
}
