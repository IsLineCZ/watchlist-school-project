﻿using System.ComponentModel.DataAnnotations;

namespace WatchList.Models
{
	public class UserComment
	{
		public int UserCommentID { get; set; }

		[Required]
		public int UserID { get; set; }

		[Required]
        [StringLength(128)]
		public string Username { get; set; }

		[Required]
		public int ShowID { get; set; }

		[Required]
        [StringLength(1024)]
		public string Text { get; set; }
	}
}
