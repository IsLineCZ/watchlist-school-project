﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WatchList.Models
{
	public class Show
	{
        [Key]
		public int ShowID { get; set; }

        [Required]
        [StringLength(128)]
		public string Name { get; set; }

        [StringLength(256)]
		public string BackdropPath { get; set; }

        [StringLength(32)]
        public string FirstAirDate { get; set; }

        [StringLength(128)]
        public string OriginalLanguage { get; set; }

        [Required]
        [StringLength(128)]
        public string OriginalName { get; set; }

        [Required]
        public string Overview { get; set; }

        [Required]
        [StringLength(64)]
        public string Genre { get; set; }

        [Required]
        public double Popularity { get; set; }

        [Required]
        [StringLength(256)]
        public string PosterUrl { get; set; }

        [Required]
        public double Rating { get; set; }

        [Required]
        public int NumberOfRatings { get; set; }

		public IEnumerable<UserComment> Comments { get; set; }
	}
}
