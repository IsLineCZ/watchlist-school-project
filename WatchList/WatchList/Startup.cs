using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WatchList.Database;
using WatchList.Services.Accounts;
using WatchList.Services.Shows;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using WatchList.Services.Logging;

namespace WatchList
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		public void ConfigureServices(IServiceCollection services)
		{
			services.AddTransient<IShowService, ShowService>();
			services.AddTransient<IAccountService, AccountService>();
			services.AddTransient<ILoggingService, LoggingService>();
			services.AddDbContext<WatchListContext>();

			services.AddAuthentication(options =>
			{
				options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
				options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
			}).AddJwtBearer(options =>
			{
				options.TokenValidationParameters = new TokenValidationParameters
				{
					ValidateIssuer = true,
					ValidateAudience = true,
					ValidateLifetime = true,
					ValidateIssuerSigningKey = true,
					ValidIssuer = "http://localhost:21772",
					ValidAudience = "http://localhost:21772",
					IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("SecretKey#12345")),
				};
			});

			services.AddCors(c => { c.AddPolicy("AllowCors", options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader()); });
			services.AddControllersWithViews().AddNewtonsoftJson(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.AddControllers();
		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			app.UseCors("AllowCors");

			_ = env.IsDevelopment() ? app.UseDeveloperExceptionPage() : app.UseExceptionHandler("/Home/Error");

			app.UseHttpsRedirection();
			app.UseRouting();
			app.UseStaticFiles();
			app.UseAuthentication();
			app.UseAuthorization();
			app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
		}
	}
}
