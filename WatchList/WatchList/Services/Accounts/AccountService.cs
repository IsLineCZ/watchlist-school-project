﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using WatchList.Database;
using WatchList.Models;
using WatchList.Services.Logging;

namespace WatchList.Services.Accounts
{
	public class AccountService : IAccountService
	{
		private WatchListContext _watchListContext;
		private readonly ILoggingService _loggingService;

		public static User loggedUser;

        public AccountService(ILoggingService loggingService)
		{
			_loggingService = loggingService;
			_watchListContext = new WatchListContext();   
        }

        public bool Register(RegistrationViewModel vm)
		{
			try
			{
				var user = _watchListContext.Users.FirstOrDefault(user => user.Username == vm.Username);
				if (user == null)
				{
					var userToAdd = new User() { Username = vm.Username, Password = vm.Password, Role = Role.User};
					_watchListContext.Users.Add(userToAdd);
					_watchListContext.SaveChanges();
					_loggingService.Log("User successfully registered.", GetUserID());
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception exception)
			{
				_loggingService.Log($"{exception.Message} ; {exception.InnerException}", GetUserID());
				return false;
			}
		}

        public User GetUser(Token token)
		{
			try
			{
				if (token.value == "null") return null;
				var userID = Int32.Parse(new JwtSecurityTokenHandler().ReadJwtToken(token.value).Claims.Where(claim => claim.Type == ClaimTypes.Name).FirstOrDefault().Value);
				var user = _watchListContext.Users.FirstOrDefault(user => user.UserID == userID);
				loggedUser = user;
				return user;
			}
			catch(Exception exception)
			{
				_loggingService.Log($"{exception.Message} ; {exception.InnerException}", GetUserID());
				return null;
			}
		}

        public string Login(LoginViewModel vm)
		{
			try
			{
				var user = _watchListContext.Users.Where(user => user.Username == vm.Username && user.Password == vm.Password).FirstOrDefault();

				if (user != null)
				{
					var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("SecretKey#123456"));
					var signingCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

					var claims = new List<Claim>
					{
						new Claim(ClaimTypes.Name, user.UserID.ToString()),
						new Claim(ClaimTypes.Role, user.Role.ToString())
					};

					var options = new JwtSecurityToken(
						issuer: "http://localhost:21772",
						audience: "http://localhost:21772",
						claims: claims,
						expires: DateTime.Now.AddMinutes(60),
						signingCredentials: signingCredentials
					);

					var token = new JwtSecurityTokenHandler().WriteToken(options);
					loggedUser = user;
					_loggingService.Log("User successfully logged in.", GetUserID());

					return token;
				}

				return string.Empty;
			}
			catch(Exception exception)
			{
				_loggingService.Log($"{exception.Message} ; {exception.InnerException}", GetUserID());
				return string.Empty;
			}
		}

		public void Logout() => loggedUser = null;

		public bool isAdmin() => loggedUser != null && loggedUser.Role == Role.Admin;

		public int? GetUserID() {
			if (loggedUser == null) return null;
			else return loggedUser.UserID;
		}
	}
}
