﻿using WatchList.Models;

namespace WatchList.Services.Accounts
{
	public interface IAccountService
	{
		public bool Register(RegistrationViewModel vm);

		public User GetUser(Token token);

		public string Login(LoginViewModel vm);

		public void Logout();

		public bool isAdmin();

		public int? GetUserID();
	}
}
