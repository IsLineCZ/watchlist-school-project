﻿using System;
using WatchList.Database;
using WatchList.Models;

namespace WatchList.Services.Logging
{
	public class LoggingService : ILoggingService
	{
		private WatchListContext _watchListContext;

		public LoggingService()
		{
			_watchListContext = new WatchListContext();
		}

		public void Log(string message, int? userID)
		{
			var logEntry = new Log()
			{
				DateCreated = DateTime.Today,
				Message = message,
				UserID = userID,
			};

			_watchListContext.Logs.Add(logEntry);
			_watchListContext.SaveChanges();
		}
	}
}
