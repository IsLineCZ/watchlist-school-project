﻿namespace WatchList.Services.Logging
{
	public interface ILoggingService
	{
		public void Log(string message, int? userID);
	}
}
