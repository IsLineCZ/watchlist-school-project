﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WatchList.Models;

namespace WatchList.Services.Shows
{
	public interface IShowService
	{
		Show GetShow(int id);

		IEnumerable<Show> GetShows(int page);

		bool DeleteShow(int id);

		bool AddShow(Show show);

		bool UpdateShow(Show show);

		void DeleteComment(int id);

		int GetShowCount();

		IEnumerable<Show> FilterShows(FilterViewModel filterParams);

		public Task FillWatchListDatabase();
	}
}
