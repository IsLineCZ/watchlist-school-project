﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using WatchList.Database;
using WatchList.Models;
using WatchList.Services.Accounts;
using WatchList.Services.Logging;

namespace WatchList.Services.Shows
{
	public class ShowService : IShowService
	{
		private WatchListContext _watchListContext;
		private readonly ILoggingService _loggingService;
		private readonly IAccountService _accountService;

		public ShowService(ILoggingService loggingService, IAccountService accountService)
		{
			_accountService = accountService;
			_loggingService = loggingService;
			_watchListContext = new WatchListContext();
		}

		public Show GetShow(int id)
		{
			return _watchListContext.Shows.Where(show => show.ShowID == id).Include(show => show.Comments).FirstOrDefault();
		}

		public IEnumerable<Show> GetShows(int page)
		{
			var offset = (page - 1) * 20;
			return _watchListContext.Shows.OrderBy(show => show.ShowID).Include(show => show.Comments).ToList().Skip(offset).Take(10);
		}

		public int GetShowCount()
		{
			return _watchListContext.Shows.Count();
		}

		public bool DeleteShow(int id) 
		{
			try
			{
				if (id > 0) 
				{
					var showToRemove = _watchListContext.Shows.Where(show => show.ShowID == id).FirstOrDefault();
					_watchListContext.Shows.Remove(showToRemove);
					_watchListContext.SaveChanges();
					_loggingService.Log($"Show with ID: {id} removed from database.", _accountService.GetUserID());
					return true;
				}

				return false;
			}
			catch(Exception exception)
			{
				_loggingService.Log($"{exception.Message} ; {exception.InnerException}", _accountService.GetUserID());
				return false;
			}
		} 

		public bool AddShow(Show show)
		{
			try
			{
				if (!ValidateShow(show)) return false;

				_watchListContext.Shows.Add(show);
				_watchListContext.SaveChanges();

				_loggingService.Log($"Show with Name: {show.Name} added to database.", _accountService.GetUserID());
				return true;
			}
			catch(Exception exception)
			{
				_loggingService.Log($"{exception.Message} ; {exception.InnerException}", _accountService.GetUserID());
				return false;
			}
		}

		public bool UpdateShow(Show show)
		{
			try
			{
				if (!ValidateShow(show)) return false;
				
				var showToUpdate = _watchListContext.Shows.Include(show => show.Comments).SingleOrDefault(s => s.ShowID == show.ShowID);
				if (showToUpdate != null)
				{
					if (show.Comments != null && show.Comments.Count() > showToUpdate.Comments.Count())
					{
						_watchListContext.Comments.Add(show.Comments.Last());
					}
					_watchListContext.Entry(showToUpdate).CurrentValues.SetValues(show);
					_watchListContext.SaveChanges();
					_loggingService.Log($"Show with ID: {show.ShowID} updated.", _accountService.GetUserID());
					return true;
				}

				return false;
			}
			catch(Exception exception)
			{
				_loggingService.Log($"{exception.Message} ; {exception.InnerException}", _accountService.GetUserID());
				return false;
			}
		}

		public void DeleteComment(int id) 
		{
			try
			{
				if (id > 0) 
				{
					var commentToRemove = _watchListContext.Comments.Where(comment => comment.UserCommentID == id).FirstOrDefault();
					_watchListContext.Comments.Remove(commentToRemove);
					_watchListContext.SaveChanges();
					_loggingService.Log($"Comment with ID: {id} deleted from database.", _accountService.GetUserID());
				}
			}
			catch(Exception exception)
			{
				_loggingService.Log($"{exception.Message} ; {exception.InnerException}", _accountService.GetUserID());
			}
		} 

		public IEnumerable<Show> FilterShows(FilterViewModel filterParams)
		{
			try
			{
				var filteredShows = _watchListContext.Shows.Where(show => 
				show.Name.ToUpper().Contains(filterParams.NormalizedName) &&
				show.Rating >= filterParams.MinRating &&
				show.Rating <= filterParams.MaxRating);

				if (filterParams.Genre != null)
				{
					filteredShows = filteredShows.Where(show => show.Genre == filterParams.Genre);
				}

				var offset = (filterParams.Page - 1) * 20;
				return filteredShows.OrderBy(show => show.ShowID).ToList().Skip(offset).Take(10);
			}
			catch(Exception exception)
			{
				_loggingService.Log($"{exception.Message} ; {exception.InnerException}", _accountService.GetUserID());
				return null;
			}
		}

		public async Task FillWatchListDatabase()
		{
			var ApiKey = "api_key=089c71412ee28afa0a54a67f6707dbc6";
			var BaseUrl = "https://api.themoviedb.org/3";
			var imageBaseUrl = "https://image.tmdb.org/t/p/w500";

			for (int i = 1; i <= 500; i++)
			{
				var ApiUrl = $"{BaseUrl}/discover/tv?sort_by=popularity.desc&page={i}&{ApiKey}";
				try
				{
					using (HttpClient client = new HttpClient())
					{
						using (HttpResponseMessage res = await client.GetAsync(ApiUrl))
						{
							using (HttpContent content = res.Content)
							{
								var data = await content.ReadAsStringAsync();
								var deserializedData = JsonConvert.DeserializeObject<MovieApiResponse>(data);

								foreach (var show in deserializedData.results)
								{

									var genre = Enum.GetName(typeof(Genre), show.genre_ids[0]);
									var showToAdd = new Show()
									{
										Name = show.name,
										Rating = show.vote_average,
										OriginalName = show.original_name,
										PosterUrl = $"{imageBaseUrl}{show.poster_path}",
										NumberOfRatings = show.vote_count,
										Overview = show.overview,
										BackdropPath = show.backdrop_path,
										FirstAirDate = show.first_air_date,
										OriginalLanguage = show.original_language,
										Popularity = show.popularity,
										Genre = genre == null ? "None" : genre.Replace("_", " "),
									};
									_watchListContext.Shows.Add(showToAdd);
									_watchListContext.SaveChanges();
								}
							}
						}
					}
				}
				catch(Exception exception)
				{
					_loggingService.Log($"{exception.Message} ; {exception.InnerException}", _accountService.GetUserID());
				}
			}

			_loggingService.Log("Table Shows filled from external API.", _accountService.GetUserID());
		}

		private bool ValidateShow(Show show)
		{
			return (
				show != null &&
				show.Name != null &&
				show.Name.Length > 0 &&
				show.Name.Length < 128 &&
				show.Rating >= 0 &&
				show.Rating <= 10 &&
				show.OriginalName != null &&
				show.OriginalName.Length > 0 &&
				show.OriginalName.Length < 128 &&
				show.PosterUrl.Length != 0 &&
				show.Genre != null &&
				show.Genre.Length != 0 &&
				show.FirstAirDate != null &&
				show.FirstAirDate.Length != 0
			);
		}
	}
}
