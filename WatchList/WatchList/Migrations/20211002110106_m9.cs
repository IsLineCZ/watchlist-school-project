﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WatchList.Migrations
{
    public partial class m9 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rating_Shows_ShowID",
                table: "Rating");

            migrationBuilder.DropForeignKey(
                name: "FK_UserComment_Shows_ShowID",
                table: "UserComment");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserComment",
                table: "UserComment");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Rating",
                table: "Rating");

            migrationBuilder.RenameTable(
                name: "UserComment",
                newName: "Comments");

            migrationBuilder.RenameTable(
                name: "Rating",
                newName: "Ratings");

            migrationBuilder.RenameIndex(
                name: "IX_UserComment_ShowID",
                table: "Comments",
                newName: "IX_Comments_ShowID");

            migrationBuilder.RenameIndex(
                name: "IX_Rating_ShowID",
                table: "Ratings",
                newName: "IX_Ratings_ShowID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Comments",
                table: "Comments",
                column: "UserCommentID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Ratings",
                table: "Ratings",
                column: "RatingID");

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_Shows_ShowID",
                table: "Comments",
                column: "ShowID",
                principalTable: "Shows",
                principalColumn: "ShowID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Ratings_Shows_ShowID",
                table: "Ratings",
                column: "ShowID",
                principalTable: "Shows",
                principalColumn: "ShowID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comments_Shows_ShowID",
                table: "Comments");

            migrationBuilder.DropForeignKey(
                name: "FK_Ratings_Shows_ShowID",
                table: "Ratings");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Ratings",
                table: "Ratings");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Comments",
                table: "Comments");

            migrationBuilder.RenameTable(
                name: "Ratings",
                newName: "Rating");

            migrationBuilder.RenameTable(
                name: "Comments",
                newName: "UserComment");

            migrationBuilder.RenameIndex(
                name: "IX_Ratings_ShowID",
                table: "Rating",
                newName: "IX_Rating_ShowID");

            migrationBuilder.RenameIndex(
                name: "IX_Comments_ShowID",
                table: "UserComment",
                newName: "IX_UserComment_ShowID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Rating",
                table: "Rating",
                column: "RatingID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserComment",
                table: "UserComment",
                column: "UserCommentID");

            migrationBuilder.AddForeignKey(
                name: "FK_Rating_Shows_ShowID",
                table: "Rating",
                column: "ShowID",
                principalTable: "Shows",
                principalColumn: "ShowID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserComment_Shows_ShowID",
                table: "UserComment",
                column: "ShowID",
                principalTable: "Shows",
                principalColumn: "ShowID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
