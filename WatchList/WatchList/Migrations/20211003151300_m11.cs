﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WatchList.Migrations
{
    public partial class m11 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "State",
                table: "Shows",
                newName: "PosterUrl");

            migrationBuilder.RenameColumn(
                name: "ImageUrl",
                table: "Shows",
                newName: "Overview");

            migrationBuilder.RenameColumn(
                name: "Genre",
                table: "Shows",
                newName: "OriginalName");

            migrationBuilder.RenameColumn(
                name: "EpisodeCount",
                table: "Shows",
                newName: "NumberOfRatings");

            migrationBuilder.AlterColumn<double>(
                name: "Rating",
                table: "Shows",
                type: "float",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<string>(
                name: "BackdropPath",
                table: "Shows",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FirstAirDate",
                table: "Shows",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OriginalLanguage",
                table: "Shows",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Popularity",
                table: "Shows",
                type: "float",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BackdropPath",
                table: "Shows");

            migrationBuilder.DropColumn(
                name: "FirstAirDate",
                table: "Shows");

            migrationBuilder.DropColumn(
                name: "OriginalLanguage",
                table: "Shows");

            migrationBuilder.DropColumn(
                name: "Popularity",
                table: "Shows");

            migrationBuilder.RenameColumn(
                name: "PosterUrl",
                table: "Shows",
                newName: "State");

            migrationBuilder.RenameColumn(
                name: "Overview",
                table: "Shows",
                newName: "ImageUrl");

            migrationBuilder.RenameColumn(
                name: "OriginalName",
                table: "Shows",
                newName: "Genre");

            migrationBuilder.RenameColumn(
                name: "NumberOfRatings",
                table: "Shows",
                newName: "EpisodeCount");

            migrationBuilder.AlterColumn<int>(
                name: "Rating",
                table: "Shows",
                type: "int",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "float");
        }
    }
}
