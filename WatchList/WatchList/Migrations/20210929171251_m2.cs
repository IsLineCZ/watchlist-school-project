﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WatchList.Migrations
{
    public partial class m2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "asd",
                table: "Shows");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "asd",
                table: "Shows",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
