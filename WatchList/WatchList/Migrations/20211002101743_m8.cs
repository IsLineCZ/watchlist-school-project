﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WatchList.Migrations
{
    public partial class m8 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rating_Shows_ShowID",
                table: "Rating");

            migrationBuilder.DropForeignKey(
                name: "FK_UserComment_Shows_ShowID",
                table: "UserComment");

            migrationBuilder.AlterColumn<int>(
                name: "ShowID",
                table: "UserComment",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ShowID",
                table: "Rating",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Rating_Shows_ShowID",
                table: "Rating",
                column: "ShowID",
                principalTable: "Shows",
                principalColumn: "ShowID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserComment_Shows_ShowID",
                table: "UserComment",
                column: "ShowID",
                principalTable: "Shows",
                principalColumn: "ShowID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rating_Shows_ShowID",
                table: "Rating");

            migrationBuilder.DropForeignKey(
                name: "FK_UserComment_Shows_ShowID",
                table: "UserComment");

            migrationBuilder.AlterColumn<int>(
                name: "ShowID",
                table: "UserComment",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "ShowID",
                table: "Rating",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Rating_Shows_ShowID",
                table: "Rating",
                column: "ShowID",
                principalTable: "Shows",
                principalColumn: "ShowID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserComment_Shows_ShowID",
                table: "UserComment",
                column: "ShowID",
                principalTable: "Shows",
                principalColumn: "ShowID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
