﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WatchList.Models;

namespace WatchList.Database
{
	public class WatchListContext : IdentityDbContext
    {   
        public DbSet<Show> Shows { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<UserComment> Comments { get; set; }

        public DbSet<Log> Logs { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
		{
            options.UseSqlServer(@"Data Source=DESKTOP-6QMNM2R; Initial Catalog=WatchList; Integrated Security=true");
		}
    }
	
}
