﻿using Microsoft.AspNetCore.Mvc;
using WatchList.Models;
using WatchList.Services.Accounts;

namespace WatchList.Controllers
{
	[Route("account")]
	[ApiController]
	public class AccountController : Controller
	{
		private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

		[HttpPost("register")]
		public IActionResult Register([FromBody] RegistrationViewModel vm)
		{
			if (vm == null ||
				vm.Password == null ||
				vm.Username == null ||
				vm.Password.Length == 0 ||
				vm.Username.Length == 0)
			return BadRequest("Username and Password must be filled to register.");

			var result = _accountService.Register(vm);
			if (result) return Ok();

			return BadRequest("Account with this username already exists.");
		}

		[HttpPost("login")]
		public IActionResult Login([FromBody] LoginViewModel vm)
		{
			if (vm == null ||
				vm.Password == null ||
				vm.Username == null ||
				vm.Password.Length == 0 ||
				vm.Username.Length == 0)
			return BadRequest("Username and password must be filled to login.");

			var token = _accountService.Login(vm);
			if (token != string.Empty) return Ok(new { token });

			return BadRequest("Wrong username or password.");
		}

		[HttpPost("logout")]
		public IActionResult Logout()
		{
			_accountService.Logout();
			return Ok();
		}

		[HttpPost("getUser")]
		public User GetUser([FromBody] Token token)
		{
			return _accountService.GetUser(token);
		}
	}
}
