﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using WatchList.Models;
using WatchList.Services.Shows;
using Microsoft.AspNetCore.Hosting;
using System.Threading.Tasks;
using WatchList.Services.Accounts;

namespace WatchList.Controllers
{
	[Route("show")]
	[ApiController]
	public class ShowController : Controller
	{
		private readonly IShowService _showService;
		private readonly IAccountService _accountService;
		private readonly IWebHostEnvironment _enviroment;

        public ShowController(IShowService showService, IWebHostEnvironment enviroment, IAccountService accountService)
        {
            _showService = showService;
			_enviroment = enviroment;
			_accountService = accountService;
        }

		[HttpPost("add")]
		public IActionResult AddShow([FromBody] Show show)
		{
			if (!_accountService.isAdmin()) return Unauthorized("Not enough permissions to do that.");
			if (_showService.AddShow(show)) return Ok();
			return BadRequest("Show not added due to validation errors.");
		} 

		[HttpPost("getShow")]
		public Show GetShow([FromBody] int id) => _showService.GetShow(id);

		[HttpGet("getShows/{page}")]
		public IEnumerable<Show> GetShows(int page) => _showService.GetShows(page);

		[HttpPost("update")]
		public IActionResult UpdateShow([FromBody] Show show) 
		{
			if (_showService.UpdateShow(show)) return Ok();
			return BadRequest("Show not updated due to validation errors.");
		}

		[HttpPost("delete/{id}")]
		public IActionResult DeleteShow(int id)
		{
			if (!_accountService.isAdmin()) return Unauthorized("Not enough permissions to do that.");
			if (_showService.DeleteShow(id)) return Ok();
			return BadRequest("Show removed because show with this is not in system or id is not valid.");
		}

		[HttpPost("deleteComment/{id}")]
		public IActionResult DeleteComment(int id)
		{
			if (!_accountService.isAdmin()) return Unauthorized("Not enough permissions to do that.");
			_showService.DeleteComment(id);
			return Ok();
		}

		[HttpGet("getShowCount")]
		public int GetShowCount() => _showService.GetShowCount();

		[HttpPost("filter")]
		public IEnumerable<Show> FilterShows([FromBody] FilterViewModel filterParams) => _showService.FilterShows(filterParams);

		[HttpPost("fillWatchListDatabase")]
		public async Task<IActionResult> FillWatchListDatabase()
		{
			if (!_accountService.isAdmin()) return Unauthorized("Not enough permissions to do that.");
			await _showService.FillWatchListDatabase();
			return Ok();
		}

		[HttpPost("saveImage")]
		public JsonResult SaveImage()
		{
			var image = Request.Form.Files[0];
			var fileName = $"{Guid.NewGuid()}{Path.GetExtension(image.FileName)}";
			var path = $"{_enviroment.WebRootPath}\\images\\{fileName}";
            
			using(var stream = new FileStream(path, FileMode.Create))
			{
				image.CopyTo(stream);
			}

			return new JsonResult($"http://localhost:21772/images/{fileName}");
		}
	}
}
