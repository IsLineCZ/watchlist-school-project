﻿CREATE PROCEDURE [dbo].[GetShows]
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
		ShowId,
		Name,
		Rating,
		Note,
		Genre,
		State,
		EpisodeCount,
		ImageUrl
	FROM Shows
	ORDER BY Rating DESC
END