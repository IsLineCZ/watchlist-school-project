﻿CREATE PROCEDURE [dbo].[AddShow]
	@Name          NVARCHAR (128),
    @Rating        INT,
    @Note          NVARCHAR (512),
    @Genre         NVARCHAR (64),
    @State         NVARCHAR (64),
    @EpisodeCount  INT,
    @ImageUrl      NVARCHAR (128)
AS
BEGIN
	SET NOCOUNT ON;
    INSERT INTO Shows (Name, Rating, Note, Genre, State, EpisodeCount, ImageUrl)
		OUTPUT INSERTED.ShowID
		VALUES (@Name, @Rating, @Note, @Genre, @State, @EpisodeCount, @ImageUrl)
END

