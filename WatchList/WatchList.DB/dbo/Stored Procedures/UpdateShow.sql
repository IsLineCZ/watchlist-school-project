﻿CREATE PROCEDURE [dbo].[UpdateShow]
    @ShowID        INT,
	@Name          NVARCHAR (128),
    @Rating        INT,
    @Note          NVARCHAR (512),
    @Genre         NVARCHAR (64),
    @State         NVARCHAR (64),
    @EpisodeCount  INT,
    @ImageUrl      NVARCHAR (128)
AS
BEGIN
	SET NOCOUNT ON;
    UPDATE Shows
    SET 
    Name = @Name, 
    Rating = @Rating,
    Note = @Note,
    Genre = @Genre,
    State = @State,
    EpisodeCount = @EpisodeCount,
    ImageUrl = @ImageUrl
    WHERE ShowID = @ShowID;
END
