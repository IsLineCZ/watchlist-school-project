﻿CREATE TABLE [dbo].[Shows]
(
	[ShowID]        INT            IDENTITY (1, 1) NOT NULL,
    [Name]          NVARCHAR (32)  NOT NULL,
    [Rating]        INT            NOT NULL,
    [Note]          NVARCHAR (64)  NULL,
    [Genre]         NVARCHAR (32)  NOT NULL,
    [State]         NVARCHAR (32)  NOT NULL,
    [EpisodeCount]  INT            NOT NULL,
    [ImageUrl]      NVARCHAR (128) NULL,
    PRIMARY KEY CLUSTERED ([ShowID] ASC)
)
